<?xml version="1.0" encoding="UTF-8"?>
<chapter xmlns="http://www.caij.qc.ca/ns/xml" xml:lang="fr" label="VI">
<info>
<title>La dévolution légale des successions</title>
<author>
<personname>
<honorific>Me</honorific>
<firstname>Marilyn</firstname>
<surname>Piccini Roy</surname></personname></author></info>
<para>Une succession 
<emphasis role="italic">ab intestat</emphasis> résulte lorsqu'une personne décède :</para>
<blockquote>
<para>-- sans testament, sans contrat de mariage ou sans contrat d'union civile contenant une donation à cause de mort ou une institution contractuelle ;</para>
<para>-- sans testament valide ; ou</para>
<para>-- dans les circonstances qui rendent impossible l'exécution des legs à cause de caducité ou de nullité (art. 736 C.c.Q.)
<footnote label="1">
<para>Pour plus de d'informations, voir aussi Jacques BEAULNE et Michel BEAUCHAMP, 
<emphasis role="italic">La liquidation des successions</emphasis>, 2
<emphasis role="superscript">e</emphasis> éd., Montréal, Wilson &amp; Lafleur, 2016, p. 64, par. 117 et p. 65, par. 118</para></footnote>.</para></blockquote>
<section label="1">
<title>1- La vocation successorale</title>
<para>L'article 653 C.c.Q. énonce la vocation successorale du conjoint survivant lié au défunt par mariage ou union civile, et celle des parents du défunt à la succession 
<emphasis role="italic">ab intestat</emphasis>. La vocation successorale du conjoint survivant n'est plus subordonnée à la renonciation à ses droits et avantages matrimoniaux, comme c'était le cas en vertu de l'ancien code selon les dispositions de l'article 624 c) C.c.B.-C. Seul le conjoint légitime a droit de participer dans la succession 
<emphasis role="italic">ab intestat</emphasis>, le conjoint de fait étant exclu et n'ayant aucun droit dans la succession 
<emphasis role="italic">ab intestat</emphasis>
<footnote label="2">
<para>Cependant, le conjoint de fait pourra avoir des droits résultant du décès en raison des dispositions de certaines lois particulières, par exemple, le droit à la rente de conjoint survivant versée par le Régime de rentes du Québec. En vertu de la 
<emphasis role="italic">Loi modifiant diverses dispositions législatives concernant les conjoints de fait</emphasis>, L.Q. 1999, c. 14, le concept de conjoint de fait a été modifié pour que les unions de fait soient reconnues sans égard au sexe des personnes ; voir Jacques BEAULNE, « Liquidateur successoral, déclaration de transmission, conjoint de fait et succession 
<emphasis role="italic">ab intestat</emphasis>, survie de l'obligation alimentaire », (1996) 2 
<emphasis role="italic">C.P. du N.</emphasis> 92 ; Marilyn PICCINI ROY et Michael PATRY, « Rights of 
<emphasis role="italic">De Facto</emphasis> Spouses in Quebec on Death », (2004) 26 
<emphasis role="italic">Estates, Trusts &amp; Pensions Journal</emphasis> 348. Voir aussi Jacques AUGER, « Les principes de désignation des héritiers légaux unité-proximité-égalité » dans Brigitte Lefebvre dir., 
<emphasis role="italic">Mélanges Roger Comtois</emphasis>, Montréal, Thémis, 2007, 74, p. 81 à 82.</para></footnote>.</para>
<para>Le divorce et la dissolution de l'union civile mettent fin aux droits de succéder, mais le conjoint séparé de corps maintient ses droits à participer dans la succession 
<emphasis role="italic">ab intestat</emphasis> de son conjoint décédé.</para></section>
<section label="2">
<title>2- La parenté
<footnote label="3">
<para>Jacques AUGER, « Les principes de désignation des héritiers légaux unité-proximité-égalité » dans Brigitte Lefebvre dir., 
<emphasis role="italic">Mélanges Roger Comtois</emphasis>, Montréal, Thémis, 2007, 74, p. 79 à 86.</para></footnote></title>
<para>La parenté est fondée sur les liens de sang ou de l'adoption (art. 655 C.c.Q.). Le degré de parenté est déterminé par le nombre de générations, chacune formant un degré. La suite des degrés forme la ligne directe ou collatérale (art. 656 C.c.Q.).</para>
<para>Les degrés ont une importance à l'intérieur d'un ordre, puisqu'ils fixent l'ordre de priorité des successibles. On peut être du même ordre, mais être exclu étant d'un degré inférieur, sauf l'exception de la représentation. Les parents au-delà du huitième degré ne succèdent pas (art. 683 C.c.Q.). À défaut d'héritier, la succession échoit à l'État (art. 653 C.c.Q.).</para></section>
<section label="3">
<title>3- Les ordres des successibles</title>
<para>Il y a trois ordres de successibles, à savoir :</para>
<para>Premier ordre : conjoint survivant et descendants (art. 666 à 669 C.c.Q.).</para>
<para>Deuxième ordre : conjoint survivant, ascendants privilégiés
<footnote label="4">
<para>Les père et mère.</para></footnote> et collatéraux privilégiés
<footnote label="5">
<para>Les frères, sœurs et leurs descendants au premier degré, soit les neveux et nièces.</para></footnote> (art. 670 à 676 C.c.Q.).</para>
<para>Troisième ordre : ascendants ordinaires
<footnote label="6">
<para>Les grands-parents, les aïeuls, les bisaïeuls, trisaïeuls, etc.</para></footnote> et collatéraux ordinaires
<footnote label="7">
<para>Oncles, cousins, petits-cousins, grands-oncles, etc. ; il faut noter qu'à l'intérieur des collatéraux ordinaires, il y a une sous-catégorie, soit les collatéraux ordinaires qui sont les descendants des collatéraux privilégiés, ce que Jacques BEAULNE, « Les successions », 
<emphasis role="italic">La réforme du Code civil -- Textes réunis par le Barreau du Québec et la Chambre des notaires du Québec</emphasis>, t. 1, Sainte-Foy, Les Presses de l'Université Laval, 1993, p. 268, appelle les collatéraux ordinaires spéciaux.</para></footnote> (art. 677 à 683 C.c.Q.).</para>
<para>Pour en faciliter la compréhension, voici sous forme de tableaux les règles de la dévolution légale prévues aux articles 666 à 683 C.c.Q.</para></section>
<section label="4">
<title>4- Les tableaux de la dévolution légale</title>
<para>
<informaltable frame="none">
<tgroup cols="4">
<colspec colnum="1" colname="col1" colwidth="25*"/>
<colspec colnum="2" colname="col2" colwidth="25*"/>
<colspec colnum="3" colname="col3" colwidth="25*"/>
<colspec colnum="4" colname="col4" colwidth="25*"/>
<tbody>
<row>
<entry namest="col1" nameend="col4" colsep="0" align="center">
<emphasis role="bold">
<emphasis role="bold">Premier ordre</emphasis></emphasis></entry></row>
<row rowsep="0">
<entry colsep="1">Articles du Code civil du Québec</entry>
<entry align="center">666</entry>
<entry align="center">667668, al. 1669</entry>
<entry align="center"/></row>
<row rowsep="1">
<entry colsep="1">Conjoint</entry>
<entry align="center">1/3</entry>
<entry align="center">X</entry>
<entry align="center">Voir 2
<emphasis role="superscript">e</emphasis> ordre</entry></row>
<row rowsep="1">
<entry colsep="1">Descendants</entry>
<entry align="center">2/3</entry>
<entry align="center">100 %</entry>
<entry align="center">X</entry></row></tbody></tgroup></informaltable></para>
<para>
<informaltable frame="all">
<tgroup cols="8">
<colspec colnum="1" colname="col1" colwidth="12*"/>
<colspec colnum="2" colname="col2" colwidth="12*"/>
<colspec colnum="3" colname="col3" colwidth="12*"/>
<colspec colnum="4" colname="col4" colwidth="12*"/>
<colspec colnum="5" colname="col5" colwidth="12*"/>
<colspec colnum="6" colname="col6" colwidth="12*"/>
<colspec colnum="7" colname="col7" colwidth="12*"/>
<colspec colnum="8" colname="col8" colwidth="16*"/>
<tbody>
<row>
<entry namest="col1" nameend="col8" colsep="0" align="center">
<emphasis role="bold">
<emphasis role="bold">Deuxième ordre</emphasis></emphasis></entry></row>
<row rowsep="0">
<entry colsep="1">Articles du Code civil du Québec</entry>
<entry align="center">674, al. 1</entry>
<entry align="center">672</entry>
<entry align="center">674, al. 2675</entry>
<entry align="center">673</entry>
<entry align="center">674, al. 2676, al. 1</entry>
<entry align="center">677</entry>
<entry align="center">671</entry></row>
<row rowsep="1">
<entry colsep="1">Conjoint</entry>
<entry align="center">X</entry>
<entry align="center">2/3</entry>
<entry align="center">X</entry>
<entry align="center">2/3</entry>
<entry align="center">X</entry>
<entry align="center">X</entry>
<entry align="center">100 %</entry></row>
<row rowsep="1">
<entry colsep="1">670, al. 1 Ascendants privilégiés</entry>
<entry align="center">1/2</entry>
<entry align="center">1/3</entry>
<entry align="center">100 %</entry>
<entry align="center">X</entry>
<entry align="center">X</entry>
<entry align="center">X</entry>
<entry align="center">X</entry></row>
<row rowsep="1">
<entry colsep="1">670, al. 2 Collatéraux privilégiés</entry>
<entry align="center">1/2</entry>
<entry align="center">-</entry>
<entry align="center">X</entry>
<entry align="center">1/3</entry>
<entry align="center">100 %</entry>
<entry align="center">X</entry>
<entry align="center">X</entry></row>
<row rowsep="1">
<entry colsep="1"/>
<entry align="center"/>
<entry align="center"/>
<entry align="center"/>
<entry align="center"/>
<entry align="center"/>
<entry align="center">Voir 3
<emphasis role="superscript">e</emphasis> ordre</entry>
<entry align="center"/></row></tbody></tgroup></informaltable></para>
<para>
<informaltable frame="all">
<tgroup cols="5">
<colspec colnum="1" colname="col1" colwidth="20*"/>
<colspec colnum="2" colname="col2" colwidth="20*"/>
<colspec colnum="3" colname="col3" colwidth="20*"/>
<colspec colnum="4" colname="col4" colwidth="20*"/>
<colspec colnum="5" colname="col5" colwidth="20*"/>
<tbody>
<row>
<entry namest="col1" nameend="col5" colsep="0" align="center">
<emphasis role="bold">
<emphasis role="bold">Troisième ordre</emphasis></emphasis></entry></row>
<row rowsep="1">
<entry colsep="1">Articles du Code civil du Québec</entry>
<entry align="center">678, al. 1</entry>
<entry align="center">678, al. 2</entry>
<entry align="center">678, al. 2</entry>
<entry align="center">653 et 696</entry></row>
<row rowsep="1">
<entry colsep="1">Descendants de collatéraux privilégiés</entry>
<entry align="center">1/2</entry>
<entry align="center">100 %</entry>
<entry align="center">X</entry>
<entry align="center">X</entry></row>
<row rowsep="1">
<entry colsep="1">Ascendants et collatéraux ordinaires</entry>
<entry align="center">1/2</entry>
<entry align="center">X</entry>
<entry align="center">100 %</entry>
<entry align="center">X</entry></row>
<row rowsep="1">
<entry colsep="1"/>
<entry align="center"/>
<entry align="center"/>
<entry align="center"/>
<entry align="center">État 100 %</entry></row></tbody></tgroup></informaltable></para></section>
<section label="5">
<title>5- La fente
<footnote label="8">
<para>Voir J. BEAULNE et M. BEAUCHAMP, 
<emphasis role="italic">La liquidation des successions</emphasis>, 2
<emphasis role="superscript">e</emphasis> éd., Montréal, Wilson &amp; Lafleur, 2016, p. 69, par. 27. Pour des explications plus approfondies, voir Jacques BEAULNE, 
<emphasis role="italic">Droit des successions</emphasis>, 5
<emphasis role="superscript">e</emphasis> éd., Montréal, Wilson &amp; Lafleur, 2016, par. 571 à 587 ; J. BEAULNE, « Les successions », 
<emphasis role="italic">La réforme du Code civil -- Textes réunis par le Barreau du Québec et la Chambre des notaires du Québec</emphasis>, t. 1, Sainte-Foy, Les Presses de l'Université Laval, 1993, p. 278 à 280 ; Albert MAYRAND, 
<emphasis role="italic">Les successions ab intestat</emphasis>, Montréal, Les Presses de l'Université de Montréal, 1971, p. 147 à 151 ; Jacques AUGER, « Les principes de désignation des héritiers légaux unité-proximité-égalité » dans Brigitte Lefebvre dir., 
<emphasis role="italic">Mélanges Roger Comtois</emphasis>, Montréal, Thémis, 2007, 74, p. 91 à 95.</para></footnote></title>
<para>Dans le cas des mariages successifs, les règles relatives à la fente déterminent les droits des demi-frères et demi-sœurs et de leurs descendants.</para>
<para>La fente est la division de la succession ou d'une partie de la succession en deux lignes : paternelle et maternelle. Selon le professeur Brière, « ce n'est cependant pas dans toutes les successions -- heureusement, pourrait-on ajouter -- que l'on a recours à ce procédé »
<footnote label="9">
<para>J. BEAULNE, 
<emphasis role="italic">Droit des successions</emphasis>, 5
<emphasis role="superscript">e</emphasis> éd., Montréal, Wilson &amp; Lafleur, 2016, par. 308.</para></footnote>.</para>
<para>Il faut distinguer trois types de fentes : premièrement, entre les collatéraux privilégiés (art. 676, al. 2 C.c.Q.), deuxièmement, entre les ascendants ordinaires et les collatéraux ordinaires (art. 679 C.c.Q.) et troisièmement, entre les collatéraux ordinaires qui descendent des collatéraux privilégiés (art. 679 C.c.Q.)
<footnote label="10">
<para>
<emphasis role="italic">Hughes (Estate of) c. Agence du revenu du Québec</emphasis>, 2015 QCCS 1925, EYB 2015-251759, confirmé en appel : 
<emphasis role="italic">Small c. Québec (Agence du revenu)</emphasis>, 2016 QCCA 632, EYB 2016-264481.</para></footnote>.</para>
<para>Dans le premier cas, la succession est divisée en deux parts égales : une part pour la ligne paternelle et l'autre pour la ligne maternelle. Les germains prennent part dans les deux lignes, les utérins dans la ligne maternelle seulement et les consanguins dans la ligne paternelle seulement.</para>
<para>En ce qui concerne les autres cas de fente, l'article 679 C.c.Q. énonce que le partage de la succession dévolue aux ascendants et aux autres collatéraux ordinaires du défunt s'opère également entre les lignes paternelle et maternelle. Dans chaque ligne, les personnes qui succèdent partagent par tête
<footnote label="11">
<para>Voir J. BEAULNE, « Les successions », 
<emphasis role="italic">La réforme du Code civil -- Textes réunis par le Barreau du Québec et la Chambre des notaires du Québec</emphasis>, t. 1, Sainte-Foy, Les Presses de l'Université Laval, 1993, p. 280, pour les problèmes concernant la fente chez les collatéraux ordinaires qui descendent des collatéraux privilégiés.</para></footnote>.</para>
<para>À titre d'exemple de la fente
<footnote label="12">
<para>Voir l'exemple dans Henri KELADA, 
<emphasis role="italic">Précis de droit québécois</emphasis>, 6
<emphasis role="superscript">e</emphasis> éd., Montréal, SOQUIJ, 1997, p. 271 et 272.</para></footnote> :</para>
<blockquote>
<para>-- Claudette épouse, en premières noces, Jean avec qui elle a deux enfants, Céline et Martin ;</para>
<para>-- Jean décède et Claudette épouse, en secondes noces, Paul avec lequel elle a deux enfants, Dominique et Laurent ;</para>
<para>-- Claudette décède et Paul épouse Suzanne en secondes noces, avec qui il a trois enfants, Alexandre, Simon et Jacques ;</para>
<para>-- Paul et Suzanne décèdent ;</para>
<para>-- Dominique décède, 
<emphasis role="italic">ab intestat</emphasis>.</para></blockquote>
<para>Céline et Martin, qui sont les enfants de Claudette, sont des parents utérins et ils prennent part dans la ligne maternelle ; Alexandre, Jacques et Simon, qui sont les enfants de Paul, sont des parents consanguins, et ils prennent part dans la ligne paternelle ; Laurent, qui est l'enfant de Claudette et de Paul, est un germain qui prend part soit dans la ligne maternelle soit dans la ligne paternelle.</para>
<para>
<informaltable frame="all">
<tgroup cols="4">
<colspec colnum="1" colname="col1" colwidth="25*"/>
<colspec colnum="2" colname="col2" colwidth="25*"/>
<colspec colnum="3" colname="col3" colwidth="25*"/>
<colspec colnum="4" colname="col4" colwidth="25*"/>
<tbody>
<row rowsep="1">
<entry colsep="1">Héritiers</entry>
<entry align="center">Ligne maternelle</entry>
<entry align="center">Ligne paternelle</entry>
<entry align="center">Total</entry></row>
<row rowsep="1">
<entry colsep="1">Céline</entry>
<entry align="center">1/6</entry>
<entry align="center">X</entry>
<entry align="center">1/6</entry></row>
<row rowsep="1">
<entry colsep="1">Martin</entry>
<entry align="center">1/6</entry>
<entry align="center">X</entry>
<entry align="center">1/6</entry></row>
<row rowsep="1">
<entry colsep="1">Laurent</entry>
<entry align="center">1/6</entry>
<entry align="center">1/8</entry>
<entry align="center">7/24</entry></row>
<row rowsep="1">
<entry colsep="1">Alexandre</entry>
<entry align="center">X</entry>
<entry align="center">1/8</entry>
<entry align="center">1/8</entry></row>
<row rowsep="1">
<entry colsep="1">Jacques</entry>
<entry align="center">X</entry>
<entry align="center">1/8</entry>
<entry align="center">1/8</entry></row>
<row rowsep="1">
<entry colsep="1">Simon</entry>
<entry align="center">X</entry>
<entry align="center">1/8</entry>
<entry align="center">1/8</entry></row></tbody></tgroup></informaltable></para></section></chapter>