<?xml version="1.0" encoding="UTF-8"?>
<chapter xmlns="http://www.caij.qc.ca/ns/xml" xml:lang="fr" label="III">
<info>
<title>La transmission de la succession</title>
<author>
<personname>
<honorific>Me</honorific>
<firstname>Marilyn</firstname>
<surname>Piccini Roy</surname></personname></author></info>
<para>La loi ne tient compte ni de l'origine ni de la nature des biens : tous ensemble, ils ne forment qu'un seul patrimoine. C'est le principe de l'unité de la succession.</para>
<section label="1">
<title>1- La saisine des héritiers, des légataires particuliers et du liquidateur</title>
<para>La transmission de la succession s'opère de plein droit. Bien qu'il soit vrai qu'aucune formalité n'est requise, il est également vrai que plusieurs exigences visent à assurer l'efficacité de la transmission telles les exigences en matière de publicité relativement aux immeubles. Les héritiers ont de plein droit la saisine de tous les biens, droits et obligations du défunt. Ils sont saisis du patrimoine du défunt par son décès ou par l'événement qui donne effet à un legs, par exemple le décès du grevé à une substitution ou l'arrivée de la date de distribution du revenu d'une fiducie.</para>
<para>Selon le professeur Mayrand, on peut définir la saisine comme le droit d'entrer en possession effective du patrimoine du défunt et d'exercer passivement et activement les actions qu'il avait
<footnote label="1">
<para>Albert MAYRAND, 
<emphasis role="italic">Les successions ab intestat</emphasis>, Montréal, Les Presses de l'Université de Montréal, 1971, p. 42 ; voir aussi Germain BRIÈRE, 
<emphasis role="italic">Les successions : La transmission de la succession -- Le mode de transmission</emphasis>, EYB1994SUC11 ; Joseph SIROIS, 
<emphasis role="italic">La succession et ses effets sur le patrimoine : étude historique et comparative</emphasis>, Montréal, Wilson &amp; Lafleur Ltée, 2001, p. 185 et 186 ; Jacques BEAULNE, « Regards croisés sur la saisine du liquidateur successoral et sur les droits des héritiers et des légataires », (2008) 110 
<emphasis role="italic">R. du N.</emphasis> 735.</para></footnote>. Certains droits d'action sont transmissibles aux héritiers. Les tribunaux ont même reconnu le droit des héritiers à certains recours au nom du défunt, tels le remboursement de frais médicaux et la réclamation pour souffrances et abrégement de la vie
<footnote label="2">
<para>Jacques BEAULNE, 
<emphasis role="italic">Droit des successions</emphasis>, 5
<emphasis role="superscript">e</emphasis> éd., Montréal, Wilson &amp; Lafleur Ltée, 2016, par. 182 ; au sujet de la transmissibilité du droit de demander la forfaiture des donations faites par contrat de mariage, voir 
<emphasis role="italic">Droit de la famille -- 2504</emphasis>, [1996] R.J.Q. 2472, EYB 1996-65462 (C.A.).</para></footnote>.</para>
<para>L'article 1441 C.c.Q. établit aussi le principe de la transmissibilité, selon lequel les héritiers d'une partie contractante, en tant que continuateurs de sa personne, sont liés par ses engagements, à moins que la nature du contrat, c'est-à-dire son caractère 
<emphasis role="italic">intuitu personae</emphasis>, ne s'y oppose.</para>
<para>Selon le troisième alinéa de l'article 625 C.c.Q., la saisine des héritiers s'étend aussi aux droits d'action du défunt contre l'auteur de toute violation d'un droit de la personnalité ou contre ses représentants (art. 10 et 35 C.c.Q.).</para>
<para>Le deuxième alinéa du même article limite toutefois la portée de la transmission des obligations du défunt, en édictant que les héritiers ne sont pas tenus des obligations du défunt au-delà de la valeur des biens qu'ils recueillent, introduisant ainsi le nouveau principe de la responsabilité limitée des héritiers pour le paiement des dettes de la succession. Par ailleurs, le fait que la saisine de l'héritier soit limitée eu égard aux obligations du défunt, sauf les exceptions prévues dans le code, cela ne modifie pas le principe voulant que l'héritier continue la personne du défunt
<footnote label="3">
<para>MINISTÈRE DE LA JUSTICE, 
<emphasis role="italic">Commentaires du ministre de la Justice</emphasis>, Le Code civil du Québec, t. 1, Québec, Les Publications du Québec, 1993, p. 372, EYB1993CM626.</para></footnote>. Cette règle bien connue de la Coutume de Paris se formulait ainsi : « Le mort saisit le vif, son hoir plus proche et habile à lui succéder. »</para>
<para>Bien que le légataire particulier qui accepte le legs ne soit pas un héritier, l'article 739, al. 1 C.c.Q. énonce qu'il est néanmoins saisi, tout comme un héritier, des biens légués.</para>
<para>Le liquidateur exerce, à compter de l'ouverture de la succession, la saisine des héritiers et des légataires particuliers sur tous les biens de la succession, meubles et immeubles (art. 777 C.c.Q.), et ce, pendant le temps nécessaire à la liquidation
<footnote label="4">
<para>Bien que la cause 
<emphasis role="italic">Hall c. Le Sous-ministre du revenu du Québec</emphasis>, [1998] 1 R.C.S. 220, REJB 1998-04667, ait pris naissance avant l'entrée en vigueur du Code civil du Québec, il est intéressant de lire les motifs du jugement accueillant l'appel que la Cour suprême du Canada a rendu et de faire un rapprochement avec les dispositions sur la saisine dans le nouveau code ; la Cour suprême a analysé la notion de saisine des légataires et des exécuteurs testamentaires et a constaté que les dispositions du nouveau code semblent avoir clarifié le droit québécois quant au fonctionnement des saisines : le nouvel article 777 C.c.Q. précise que, pendant l'administration de la succession, c'est le liquidateur qui a priorité sur les légataires et les héritiers ; voir également Julie LEBREUX, « L'imposition du revenu d'une succession à la lumière de l'arrêt 
<emphasis role="italic">Mary Margaret Hall c.</emphasis> 
<emphasis role="italic">Le Sous-ministre du revenu du Québec</emphasis> », (1997) 100 
<emphasis role="italic">R . du N</emphasis>. 99 ; 
<emphasis role="italic">Borduas (Succession de) c. Denis</emphasis>, 2013 QCCA 349, EYB 2013-218588 ; 
<emphasis role="italic">Hamel c. Hamel</emphasis>, 2015 QCCS 5682, EYB 2015-259498 (appel rejeté, 2016 QCCA 448, EYB 2016-263267) ; 
<emphasis role="italic">Côté c. Côté</emphasis>, 2017 QCCS 1808, EYB 2017-279375, où la cour a déterminé que tant que la succession n'est pas liquidée, les héritiers ne possèdent ni la qualité ni l'intérêt pour réclamer personnellement à la liquidatrice les biens de la succession
<emphasis role="italic"> ; R &amp; G Trust c. Gold</emphasis>, 2018 QCCS 208, EYB 2018-289798, où la cour a réitéré que pendant la liquidation, seule la liquidatrice a l'autorité d'effectuer des réclamations patrimoniales associées avec la 
<emphasis role="italic">seisin</emphasis> d'une succession.</para></footnote>. Il est intéressant de noter les propos d'un auteur sur la saisine du liquidateur :</para>
<blockquote>
<para>« La rédaction de l'article 777 laisse clairement entendre qu'il ne s'agit pas ici d'une saisine qui se superpose ou se substitue à celle des héritiers et légataires particuliers. Pendant la liquidation, ces derniers ne peuvent en conséquence exercer les droits que leur confère la saisine. »
<footnote label="5">
<para>Dominique DUCLOS, « Aperçu des règles de la transmission et de liquidation du nouveau Code civil et la responsabilité des héritiers à l'égard des dettes », 
<emphasis role="italic">La planification successorale et le nouveau Code civil, conférence de l'Institut Canadien</emphasis>, 26 septembre 1994, p. 5.</para></footnote></para></blockquote>
<para>La saisine du liquidateur n'est pas limitée dans le temps ni à certains biens. Une clause dans un testament qui a pour effet de restreindre la durée ou l'étendue de la saisine légale du liquidateur de manière à empêcher un acte nécessaire à la liquidation est réputée non écrite (art. 778 C.c.Q.). Le liquidateur peut même revendiquer les biens contre les héritiers et les légataires (art. 777, al. 2 C.c.Q.).</para></section>
<section label="2">
<title>2- La pétition d'hérédité</title>
<para>L'article 626 C.c.Q. établit le délai pendant lequel un successible peut faire reconnaître sa qualité d'héritier
<footnote label="6">
<para>Dans l'affaire 
<emphasis role="italic">A.J.C. (Succession d')</emphasis>, 2004BE-639, EYB 2004-61993 (C.S.), la Cour supérieure définit l'action en pétition d'hérédité comme ayant « uniquement pour but de déterminer lequel des adversaires (à l'action) a vraiment la qualité d'héritier ou de légataire ».</para></footnote>. Il énonce qu'un successible peut toujours faire reconnaître sa qualité d'héritier, dans les dix ans qui suivent soit l'ouverture de la succession à laquelle il prétend avoir droit, soit le jour où son droit s'est ouvert
<footnote label="7">
<para>
<emphasis role="italic">S. (S.) c. Québec (Curateur public)</emphasis>, J.E. 2004-730, REJB 2004-54505 (C.S.). Voir aussi 
<emphasis role="italic">McLennan (Succession de) c. Québec (Sous-ministre du Revenu)</emphasis>, 2009 QCCA 1521, EYB 2009-162585 (demande d'autorisation d'appel à la Cour suprême rejetée, n
<emphasis role="superscript">o</emphasis> 33398, 14-01-2010) : le délai de 10 ans pour intenter l'action en pétition d'hérédité est un délai de déchéance, même le défaut par le Curateur public de publier les avis publics requis par la loi ne peut servir pour relever les requérants de leur défaut d'agir à l'intérieur de ce délai (demande pour permission d'appel à la Cour suprême du Canada rejetée) ; 
<emphasis role="italic">Deshaies c. Québec (Sous-ministre du Revenu)</emphasis>, 2010 QCCA 905, EYB 2010-173632 (demande d'autorisation d'appel à la Cour suprême du Canada rejetée). Dans le cas d'une succession 
<emphasis role="italic">ab intestat</emphasis>, le droit des successibles de second rang et de rangs subséquents s'ouvre au terme du délai de 10 ans pendant lequel les successibles de premier rang pouvaient faire reconnaître leur qualité d'héritier ; 
<emphasis role="italic">Colizza c. Québec (Agence du revenu)</emphasis>, 2015 QCCA 1285, EYB 2015-255133 ; 
<emphasis role="italic">Lacharité c. Leclerc</emphasis>, 2016 QCCS 6017, EYB 2016-273866 ; 
<emphasis role="italic">Agence du revenu du Québec c. Small</emphasis>, 2016 QCCA 632, EYB 2016-264481.</para></footnote>. Par exemple, les héritiers testamentaires pourront intenter une action en pétition d'hérédité lorsque quelqu'un décède sans testament, mais que quatre ans plus tard on découvre un testament olographe dans lequel les personnes nommées à recueillir la succession n'étaient pas les héritiers légaux. Préalablement à cette démarche, le testament olographe invoqué devra être vérifié par le tribunal.</para>
<para>L'action en pétition d'hérédité est introduite par une demande introductive d'instance (art. 141 C.p.c.) et les héritiers ou les légataires particuliers apparents doivent être assignés
<footnote label="8">
<para>Voir aussi la 
<emphasis role="italic">Loi sur l'application de la réforme du Code civil</emphasis>, L.Q. 1992, c. 57, art. 39, al. 2.</para></footnote>.</para></section>
<section label="3">
<title>3- L'héritier apparent</title>
<para>Les articles 627 à 629 C.c.Q. traitent des effets de l'action en pétition d'hérédité en ce qui concerne les rapports entre les héritiers véritables et les héritiers apparents de bonne ou de mauvaise foi ou les tiers. L'héritier apparent doit rendre ce qu'il détient à l'héritier véritable, suivant les règles relatives à la restitution des prestations prévues aux articles 1699 à 1707 C.c.Q.</para></section></chapter>