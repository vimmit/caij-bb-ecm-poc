<?xml version="1.0" encoding="UTF-8"?><article>
	
   
<section role="1">
<title>1- La compétence de la cour</title>
<para>Les règles concernant la tutelle aux mineurs se trouvent au Code civil du Québec. Les articles 177 à 255 C.c.Q. complètent le régime prévu par la <emphasis role="italic">Loi sur la protection de la jeunesse</emphasis>. Ainsi, généralement, la Cour supérieure a compétence à l'égard de ces cas. Toutefois, les dispositions de la <emphasis role="italic">Loi sur la protection de la jeunesse</emphasis> permettent à un juge de la Cour du Québec, Chambre de la jeunesse, de rendre une ordonnance lorsque la situation de l'enfant est prise en charge par le directeur de la protection de la jeunesse (ci-après « DPJ ») (art. 70.1 <emphasis role="italic">L.p.j.</emphasis>)<footnote label="1"><para><emphasis role="italic">Protection de la jeunesse -- 091416</emphasis>, 2009 QCCQ 7251, EYB 2009-163368.</para></footnote> et correspond aux conditions de l'article 207 C.c.Q. :</para>
<para role="Citation;">-- lorsque l'enfant est orphelin et n'est pas pourvu d'un tuteur ;</para>
<para role="Citation;">-- lorsque ni le père ni la mère n'assume, de fait, le soin, l'entretien ou l'éducation de l'enfant ;</para>
<para role="Citation;">-- lorsque l'enfant serait vraisemblablement en danger s'il retournait auprès de ses parents.</para>
</section>
<section role="1">
<title>2- La nomination d'un tuteur et ses effets</title>
<para>Lorsqu'un enfant est pris en charge par le DPJ, le juge de la Chambre de la jeunesse peut alors nommer une personne recommandée par le DPJ (ou le DPJ lui-même) tuteur à l'enfant (art. 70.1 <emphasis role="italic">L.p.j.</emphasis>). Il s'agit là d'un projet de vie permanent et stable pour l'enfant puisque le tribunal nomme un tuteur personnellement et que cette charge n'est pas transférable<footnote label="2"><para><emphasis role="italic">Protection de la jeunesse -- 09</emphasis>2, 2009 QCCQ 651, EYB 2009-155103.</para></footnote>.</para>
<para>Après la nomination du tuteur, le DPJ met fin à son intervention et l'enfant est pris en charge par le tuteur (art. 70.2 <emphasis role="italic">L.p.j.</emphasis>). Si le DPJ est lui-même désigné comme tuteur à l'enfant, il maintient son intervention en vertu de la <emphasis role="italic">Loi sur la protection de la jeunesse</emphasis><footnote label="3"><para><emphasis role="italic">Protection de la jeunesse -- 10579</emphasis>, 2010 QCCQ 10486.</para></footnote>.</para>
<para>La loi prévoit une aide financière pour une personne nommée tuteur à un enfant (art. 70.3 <emphasis role="italic">L.p.j.</emphasis>)<footnote label="4"><para><emphasis role="italic">Règlement sur l'aide financière pour favoriser la tutelle à un enfant</emphasis>, RLRQ, c. P-34.1, r. 5.</para></footnote>.</para>
</section>
<section role="1">
<title>3- Le remplacement d'un tuteur</title>
<para>Le tribunal peut être saisi d'une demande en remplacement de tuteur, notamment en cas de décès du tuteur ou si ce dernier a des motifs sérieux de ne plus exercer cette responsabilité ou qu'il n'est plus en mesure de le faire (art. 70.4 <emphasis role="italic">L.p.j.</emphasis>). Cette demande peut être présentée par le tuteur, par une personne intéressée, par le DPJ ou par le curateur public. Le tribunal exigera alors du DPJ qu'il procède à une évaluation de la situation sociale de l'enfant (art. 70.4, al. 2 <emphasis role="italic">L.p.j</emphasis>.).</para>
</section>
<section role="1">
<title>4- Le rétablissement du parent dans sa charge tutélaire</title>
<para>Un parent peut s'adresser au tribunal pour être rétabli dans sa charge tutélaire (art. 70.5 <emphasis role="italic">L.p.j</emphasis>.). Le parent doit démontrer que sa demande va dans le sens de l'intérêt de l'enfant et que ce dernier ne se trouve plus dans une des situations de l'article 207 C.c.Q. Dans ce cas, le DPJ doit fournir au tribunal une évaluation de la situation de l'enfant (art. 70.5, al. 2 <emphasis role="italic">L.p.j.</emphasis>).</para>
</section>
<section role="1">
<title>5- Les mesures accessoires à la tutelle</title>
<para>Le tribunal peut prévoir toute mesure relative à la tutelle dans l'intérêt de l'enfant, par exemple le maintien des relations personnelles entre l'enfant et les membres de sa famille et les modalités qui en découlent (art. 70.6 <emphasis role="italic">L.p.j.</emphasis>)<footnote label="5"><para><emphasis role="italic">Protection de la jeunesse -- 105880</emphasis>, 2010 QCCQ 16814.</para></footnote>.</para>
</section>
</article>